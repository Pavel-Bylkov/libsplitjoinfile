//
// Created by MacBook on 12.11.2021.
//
#include <iostream>
#include "LibSJ.hpp"

void test_get_size() {
    std::cout << "func get_size - ";
    std::cout << (( get_size(" 1024") == 1024 && get_size(" 1024 B") == 1024 &&
                    get_size(" 1024K") == 1048576 && get_size(" 100 M") == 104857600 &&
                    get_size(" 10G") == 10737418240ull) ? "OK" : "KO") << std::endl;
}

void test_replace_name() {
    std::cout << "func replace_name - ";
    try {
        YAML::Node config = YAML::Load("[{plot_number: 1, plot_name: a}, {plot_number: 2, plot_name: b}]");
        std::ofstream fout("test.yaml");
        fout << config;
        fout.close();
    } catch(const YAML::ParserException& ex) {
        std::cout << ex.what() << std::endl;
    }

    replace_name("test.yaml", "bbb", 1);

    string inbuf, inbuf2;
    try {
        YAML::Node config = YAML::LoadFile("test.yaml");
        for (std::size_t i = 0; i < config.size(); ++i) {
            if (config[i]["plot_number"].as<int>() == 1) {
                inbuf = config[i]["plot_name"].as<std::string>();
            }
            if (config[i]["plot_number"].as<int>() == 2) {
                inbuf2 = config[i]["plot_name"].as<std::string>();
            }
        }
    } catch(const YAML::ParserException& ex) {
        std::cout << ex.what() << std::endl;
    }
    std::remove("test.yaml");
    std::cout << ((inbuf == "bbb" &&
        inbuf2 == "b") ? "OK" : "KO") << std::endl;
}

int main()
{
    std::cout << "Tests start...\n";

    test_get_size();
    test_replace_name();

    SJF::split("config.yaml", "/Users/macbook/2ЗРС.mp4", 1);

//    FileSplitJoin res;
//
//    res.set_config("config.yaml", "join");
//    res.join("test.mp4");

    return 0;
}

