#include "LibSJ.hpp"

// Return size each part plot-file in bytes from string format
uint64_t get_size(const char *str) {
    uint64_t size = std::strtoull(str, nullptr, 10);
    int i = 0;

    while (str[i] && (str[i] == ' ' || std::isdigit(str[i])))
        ++i;

    if (str[i]) {
        string str2 ("BKMGTPE");
        std::size_t found = str2.find(str[i]);
        if (found != std::string::npos)
            for ( ;found > 0; --found)
                size *= 1024;
    }
    return size;
}

// Add or Replace plot_name param in config yaml file for current plot_number
bool replace_name(const string &config, const string &filename, int n) {
    try {
        YAML::Node node = YAML::LoadFile(config);
        for (std::size_t i = 0; i < node.size(); ++i) {
            if (node[i]["plot_number"].as<int>() == n) {
                node[i]["plot_name"] = filename;
            }
        }
        std::ofstream fout(config);
        fout << node;
        fout.close();
        return true;
    } catch(const YAML::ParserException& ex) {
        std::cerr << ex.what() << std::endl;
    }
    return false;
}

void FileSplitJoin::set_config(const string &config, const string &mode, int n)
{
    auto res = (mode == "split" ?
        set_for_split(config, n) : set_for_join(config, n));
    if (res)
        std::cout << "Setting config successfully completed.\n";
    else {
        std::cerr << "Setting config completed with error.\n";
        throw std::runtime_error("set config failed");
    }
    cfg_name = config;
}

int FileSplitJoin::set_for_split(const string &config, int n)
{
    string name;
    try {
        YAML::Node node = YAML::LoadFile(config);
        for (std::size_t i = 0; i < node.size(); ++i) {
            if (node[i]["plot_number"].as<int>() == n) {
                plot_name = node[i]["plot_name"].as<string>();
                plot_size = filesize(plot_name.c_str());
                if (plot_size == 0)
                    throw std::runtime_error("plot size failed");
                name = plot_name.substr(plot_name.find_last_of("/\\") + 1);

                part_size = get_size(node[i]["part_size"].as<string>().c_str());
                size_t n_parts = size_t(plot_size / part_size) + 1;
                if (plot_size && part_size)
                    names = new string[n_parts];

                if (!name.empty() && part_size && node[i]["disk_path"]) {
                    for (std::size_t j = 0; j < node[i]["disk_path"].size(); ++j) {
                        if (node[i]["disk_path"][j]["path"] && j < n_parts) {
                            names[j] += node[i]["disk_path"][j]["path"].as<string>();
                            fs::create_directories(names[j]);
                            names[j] += "/" + name + ".prt";
                            names[j] += std::to_string(j + 1);
                        }
                    }
                }
            }
        }
        return 1;
    } catch(const YAML::ParserException& ex) {
        std::cerr << ex.what() << std::endl;
    }
    return 0;
}

int FileSplitJoin::set_for_join(const string &config, int n)
{
    string name;
    try {
        YAML::Node node = YAML::LoadFile(config);
        for (std::size_t i = 0; i < node.size(); ++i) {
            if (node[i]["plot_number"].as<int>() == n) {
                plot_name = node[i]["plot_name"].as<string>();

                name = plot_name.substr(plot_name.find_last_of("/\\") + 1);
                part_size = get_size(node[i]["part_size"].as<string>().c_str());
                if (node[i]["disk_path"])
                    names = new string[node[i]["disk_path"].size()];

                if (!name.empty() && node[i]["disk_path"]) {
                    for (std::size_t j = 0; j < node[i]["disk_path"].size(); ++j) {
                        if (node[i]["disk_path"][j]["path"]) {
                            names[j] += node[i]["disk_path"][j]["path"].as<string>();
                            names[j] += "/" + name + ".prt";
                            names[j] += std::to_string(j + 1);
                            plot_size += filesize(names[j].c_str());
                        }
                    }
                }
            }
        }
        return 1;
    } catch(const YAML::ParserException& ex) {
        std::cerr << ex.what() << std::endl;
    }
    return 0;
}

void FileSplitJoin::split()
{
    if (cfg_name.empty()) return;
    size_t buf_size = 100000;
    uint64_t count = 0;
    std::FILE *fw;
    auto *x = new unsigned char[buf_size];

    std::FILE* plot_file;
    fopen_ex(&plot_file, plot_name, "rb");
    int i = 0;
    while (!std::feof(plot_file))
    {
        std::cout << "Create " << names[i] << "\n";
        fopen_ex(&fw, names[i++],"wb");
        while (!std::feof(plot_file) && count != part_size) {
            size_t tmp = (count + (uint64_t)buf_size <= part_size) ? buf_size : (part_size - count);
            std::fread(x, 1, tmp, plot_file);
            count += fwrite_ex(fw, x, tmp);
        }
        count = 0;
        std::fclose(fw);
    }
    std::fclose(plot_file);
    delete [] x;
}

int SJF::split(const string &config, const string &filename, int n)
{
    try {
        FileSplitJoin plot;
        replace_name(config, filename, n);
        plot.set_config(config, "split", n);
        plot.split();
    }
    catch (std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 0;
    }
    return 1;
}
