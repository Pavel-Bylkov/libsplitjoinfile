#ifndef SPLITJOIN_LIBSJ_HPP
#define SPLITJOIN_LIBSJ_HPP

/*
 * ToDo Replace yaml parser from https://github.com/jbeder/yaml-cpp
 * tutorial https://github.com/jbeder/yaml-cpp/wiki/Tutorial
 */

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cassert>
#include <string>
#include <cstring>
#include <exception>
#include <cstdint>
#include "yaml.h"
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

typedef std::FILE* file_ptr;

using std::string;

inline
uint64_t filesize(const char* filename)
{
    std::ifstream in_file(filename, std::ios::binary);
    in_file.seekg(0, std::ios::end);
    uint64_t file_size = in_file.tellg();
    in_file.close();
    return file_size;
}

inline
uint64_t fwrite_ex(std::FILE* file, const void* buf, size_t length) {
    if(std::fwrite(buf, 1, length, file) != length) {
        throw std::runtime_error("fwrite() failed");
    }
    return (uint64_t)length;
}

inline
void fopen_ex(file_ptr *file, const string & fname, const char *mode) {
    *file = std::fopen(fname.c_str(), mode);
    if (!(*file)) {
        throw std::runtime_error("fopen() failed");
    }
}

// Return size each part plot-file in bytes from string format
uint64_t get_size(const char *str);

// Add or Replace plot_name param in config yaml file for current plot_number
bool replace_name(const string &config, const string &filename, int n);


class FileSplitJoin {
public:
    FileSplitJoin(): cfg_name(""), names(nullptr), part_size(0), plot_size(0),
                     files(nullptr), plot_name(""), plot_seek(0) {}

    void set_config(const string &config, const string &mode, int n);
    int set_for_split(const string &config, int n);
    int set_for_join(const string &config, int n);
    
    
    ~FileSplitJoin()
    {
        delete [] names;
        if (files) free(files);
    }

    void split();
    void join(const string &filename)
    {
        (void)filename;
    }

    uint64_t read() { return plot_seek;}


private:
    string cfg_name;
    string *names;
    uint64_t part_size;
    uint64_t plot_size;
    file_ptr *files;
    string plot_name;
    uint64_t plot_seek;
};


inline
void fseek_set(FILE* file, uint64_t offset) {
    if(fseek(file, offset, SEEK_SET)) {
        throw std::runtime_error("fseek() failed");
    }
}



inline
size_t fwrite_at(FILE* file, uint64_t offset, const void* buf, size_t length) {
    fseek_set(file, offset);
    fwrite_ex(file, buf, length);
    return length;
}


namespace SJF {
    int split(const string &config, const string &filename, int n);
}


#endif //SPLITJOIN_LIBSJ_HPP
