NAME = test_split_join

CC = clang++

CFLAG =  -Wall -Wextra -Werror

INCLUDES = LibSJ.hpp
SRCS = LibSJ.cpp main.cpp
LIB = -L/usr/local/Cellar/yaml-cpp/0.6.3_1/lib -lyaml-cpp
LIB += -L/usr/local/Cellar/boost/1.76.0/lib -lboost_system -lboost_filesystem
HEADERS = -I/usr/local/Cellar/yaml-cpp/0.6.3_1/include/yaml-cpp
HEADERS += -I/usr/local/Cellar/yaml-cpp/0.6.3_1/include
HEADERS += -I/usr/local/Cellar/boost/1.76.0/include/

all: $(NAME)

$(NAME): $(INCLUDES)
	$(CC) $(SRCS) $(LIB) $(HEADERS) -o $(NAME) $(CFLAG) -std=c++11


fclean:
	@rm -f $(NAME)

re: fclean all

.PHONY: all fclean re